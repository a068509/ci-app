# TP #6 - Pipeline avec tests d'intégration

> **Objectifs du TP**
> * Effectuer des tests qui necessitent le démarrage de l'application
> * Demarrer une ressource requise par notre application au démarrage (un base de donnée) grace à Gitlab et à Docker
> 
> **Prérequis**
> Avant de commencer ce TP, vous devez avoir satisfait les prérequis suivants
> * Vous avez validé le TP précédent
> 
> **Niveau de difficulté :** Intermédiaire

## Répertoire de travail

Nous allons commencer par nous assurer que nous sommes bien dans notre répertoire de travail `/home/ubuntu/ci-app`.

```bash
$ pwd
```

## Le plan d'attaque

Nous avons ajouté des tests qui ne necessitent pas de démarrer l'application. Il est maintenant temps
de tester l'application pendant son runtime. Nous allons utiliser Docker pour packager notre application
et déclencher son execution.
Enfin nous ajouterons des labels sur notre pipeline.

Notre premiere étape consistera donc a créer une image docker contenant notre application.

## Ajout d'une étape de build d'un container

La création d'une image passe par l'écriture d'un Dockerfile. Le sujet du TP n'étant pas de créer
une image le Dockerfile est déjà présent dans le dépôt à la racine du projet.

Ajoutons un job dans le pipeline permettant la création d'une image docker.
Nous n'utiliserons pas Docker pour créer l'image, nous utiliserons Kaniko qui permet de créer une
image sans accès au daemon docker (donc sans ouvrir la socket).

Ajoutez le code suivant a votre pipeline :

```yaml
variables:
  CONTAINER_COMMIT_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  CONTAINER_LATEST_IMAGE: $CI_REGISTRY_IMAGE:latest
  PYTHON3_IMAGE: python:alpine

stages:
- syntax
- unittests
- sast
- build

[...]
Build Image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - test -z "$DOCKER_CONFIG" && export DOCKER_CONFIG=/kaniko/.docker
    - |
      cat > "${DOCKER_CONFIG}/config.json" <<EOF
      {
        "auths": {
          "${CI_REGISTRY}": {
            "username": "gitlab-ci-token",
            "password": "${CI_JOB_TOKEN}"
          }
        }
      }
      EOF
    - >-
        /kaniko/executor
        --context "${CI_PROJECT_DIR}"
        --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
        --build-arg APP_VERSION="${CI_COMMIT_SHORT_SHA}"
        --destination "${CONTAINER_COMMIT_IMAGE}"
        --destination "${CONTAINER_LATEST_IMAGE}"
```

Envoyez votre code en revue en poussant votre code sur une branche :

```
$ git add .gitlab-ci.yml
$ git commit -m "Ajout d'un stage pour la création d'une image docker"
$ git checkout -b add_docker_build
$ git push origin add_docker_build
```
Créez la Merge Request est attendez l'execution du pipeline.

> Question 7.1
>
> - Sauriez vous expliquer la manipulation que nous effectuons avec le fichier config.json ?
> - Pourquoi effectuons-nous cette opération ?
> - Comment pouvons-nous verifier si l'image est bien disponible ?

> Reponses
>
> - Nous créons un fichier de configuration pour Kaniko contenant les crédentials permettant l'authent sur le registre docker.
> - Le job de CI n'est pas authentifier automatiquement avec le regitry.
> - Consultation du registry sur Gitlab ou `docker pull` de l'image depuis la console, après s'être authentifié avec les credentials de notre utilisateur.

Vous pouvez maintenant tester le démarrage de l'application en démarrant un container utilisant votre image :

```
# docker run --rm -it -d -p 8000:8000 registry.gitlab.com/<votre_login_gitlab>/ci-app:latest
```

Vous pouvez maintenant tester l'application en envoyant une requête à l'application :

```
# curl 127.0.0.1:8000
```

Celle-ci vous répondra `Hello, App!` si tout fonctionne correctement.

Une fois ce test concluant vous pouvez merger votre Merge Request.
La suite du TP sera effectuée sur une autre MR.

## Création de tests d'integration pour notre application

Dans l'étape précédente nous avons créé un artefact (l'image de container) contenant notre application.
Nous souhaitons maintenant verifier que cet artefact est fonctionnel, nous allons donc le tester a travers
un nouveau job dans notre pipeline.

Nous avons utiliser le hash du commit comme tag pour notre image, il sera donc facile de retrouver notre image
à l'aide de la variable d'environnement `CI_COMMIT_SHORT_SHA` disponible dans les job Gitlab-CI.

Pour effectuer notre test, nous allons créer des tests effectuant des requêtes HTTP sur notre application
qui sera démarrée dans un container basé sur l'image créée dans le job précedent.

Commençons par créer le fichier `gitlab-ci-scripts/test_bats.sh` contenant les tests que nous souhaitons exécuter :

```
#!/usr/bin/env bats

@test "Check Home page" {
  curl -sfq my-app:8000/ | fgrep Hello
}

@test "Check version URL" {
  curl -sfq my-app:8000/version
}

@test "Check version URL Content" {
  curl -sfq my-app:8000/version | fgrep $CI_COMMIT_SHORT_SHA
}
```

Donnons les droits d'éxecution au script :

```
chmod 740 test_bats.sh
```

Créons maintenant le job qui executera ces tests dans le pipeline :

```yaml
stages:
  - syntax
  - sast
  - unittests
  - build
  - inttests

[...]

Integ tests:
  image: dduportal/bats:0.4.0
  stage: inttests
  script:
  - $CI_PROJECT_DIR/gitlab-ci-scripts/test_bats.sh
```

> Question :
> 
> - Pourquoi avons-nous changer la valeur du paramètre `image` ?

Nous devons maintenant demander à Gitlab de démarrer notre application avant d'exécuter notre test.
Nous allons utiliser le paramètre `services` qui permet de spécifier une liste d'images docker à démarrer avant d'exécuter
les commandes spécifiées dans le paramètre `script`.

```
Integ tests:
  image:
    name: dduportal/bats:0.4.0
    entrypoint: [""]
  stage: inttests
  services:
  - name: $CONTAINER_COMMIT_IMAGE
    alias: my-app
  script:
  - $CI_PROJECT_DIR/gitlab-ci-scripts/test_bats.sh
```

En précisant un `alias` pour notre image, nous serons en capacité d'utiliser l'alias comme un nom d'hôte pour atteindre l'application.
Vous aurez peut-être remarqué que les tests que nous avons écrits dans `gitlab-ci-scripts/test_bats.sh` pointent une URL contenant `my-app:8000`
ce qui correspond déjà à la valeur de l'alias que nous utilisons ici.

Soumettez votre changement dans une nouvelle Merge Request :

```
$ git checkout -b add integration_tests
$ git add .gitlab-ci.yml gitlab-ci-scripts/test_bats.sh
$ git commit -m "Ajout d'un job executant les tests d'integration"
$ git push origin integration_tests
```

Les tests doivent passer sans erreurs.

## Merge de votre changement

Ce TP est maintenant terminé. Dans un contexte réel il serait temps de demander une revue de votre code à l'un de vos pair.
Dans le cadre de cette formation, vous pouvez "merger" vous même vos changements.

Retournez dans l'interface Gitlab sur votre Merge Request et cliquez sur le bouton "Merge".
