# TP #5 - Premier pipeline

> **Objectifs du TP**
> * Ecrire un premier pipeline permettant de valider la conformité du code.
> * Apprendre et manipuler `.gitlab-ci.yml`
> 
> **Prérequis**
> Avant de commencer ce TP, vous devez avoir satisfait les prérequis suivants
> * Vous avez validé votre accès à Gitlab
> * Vous avez ajouté votre clé SSH a votre compte
> 
> **Niveau de difficulté :** Débutant

## Fork du projet

Rendez-vous à l'URL suivante : `https://gitlab.com/amazin/ci-app` où vous trouverez un projet contenant
une application d'exemple que nous allons utiliser pendant les TPs.

Cliquez sur le bouton "Fork" en haut de la page du projet afin de créer une copie du projet dans votre namespace.
Vous devriez arriver sur la page de la copie du projet.

Aller dans "Settings > Repository > Default branch", selectionner "eleve" puis cliquer sur "Save changes", afin de travailler depuis cette branche principalement.

Revenez sur la page d'accueil du projet et cliquez sur le bouton `Clone` en haut de la page et copiez l'URL
`Clone with SSH`.
Collez ensuite l'URL dans la console que nous vous avons fournit et ajoutant `git clone` devant l'URL :

```
# git clone git@gitlab.com:<votre_login>/ci-app.git
```

Dans tous les TPs qui suivent vous travaillerez sur ce Fork du projet qui vous permettra de travailler en isolation.

## Répertoire de travail

Nous allons commencer par nous assurer que nous sommes bien dans notre répertoire de travail `/home/ubuntu/ci-app`.

```bash
$ pwd
```

Vérifiez que votre répertoire courant est bien `/home/ubuntu/ci-app`.

## Création d'un pipeline avec un job

Nous allons créer notre premier pipeline. Pour cela rien de plus simple, il suffit de créer un fichier `.gitlab-ci.yml` à la racine du projet Git.
Le pipeline ne contiendra qu'un seul job, qui consiste à analyser la conformité du code avec les règles PEP8 (le standard en Python).

Créons le fichier `.gitlab-ci.yml` suivant:

```yaml
variables:
  PYTHON3_IMAGE: python:alpine

stages:
  - syntax

PyCodeStyle:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
  - pip install pycodestyle
  - pycodestyle app.py
```

Ce pipeline défini une étape `syntax` qui execute un job `PyCodeStyle` executant la commande `pycodestyle`.
Ajoutez ce fichier a vos sources en effectuant un commit :

```
$ git add .gitlab-ci.yml
$ git commit -m "Ajout d'un pipeline testant la conformité pep8"
```

Envoyez votre code en revue en poussant votre code sur une branche :

```
$ git checkout -b add_pipeline_conformity
$ git push origin add_pipeline_conformity
```
La commande `push` vous renvoie une URL permettant la création automatique d'une Merge Request, exemple :

```
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 1.17 KiB | 1.17 MiB/s, done.
Total 4 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for add_pipeline_conformity, visit:
remote:   https://gitlab.com/rrey-octo/ci-app/merge_requests/new?merge_request%5Bsource_branch%5D=add_pipeline_conformity
remote:
To gitlab.com:rrey-octo/test-gitlab-tp.git
 * [new branch]      add_pipeline_conformity -> add_pipeline_conformity
```

Cliquez sur le lien qui vous amène sur un formulaire de création de Merge Request sur Gitlab et validez la création.

**Attention !** Lors de la création de la Merge Request, Gitlab vous proposera automatiquement de faire la MR sur
le projet d'origine et non pas votre Fork, il faudra donc corriger la branche de destination pour utiliser votre fork !

> Question 6.1
>
> - Pourquoi le pipeline a t-il échoué ?

Corrigez les erreurs remontées dans la console du job `PyCodeStyle` et effectuez un nouveau changement :

```
$ git add <lefichier>
$ git commit -m "Corrections pep8"
$ git push origin add_pipeline_conformity
```

## Création d'un second job parallele

Il existe d'autres linter que `pycodestyle` pour Python, `pylint` est un autre linter populaire.
Nous pourrions utiliser les 2 outils et executer les deux jobs en parallele, pour cela éditons notre `.gitlab-ci.yml` :

```yaml
variables:
  PYTHON3_IMAGE: python:alpine

stages:
  - syntax

PyCodeStyle:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
  - pip install pycodestyle
  - pycodestyle app.py

Pylint:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
  - pip install pylint
  - pylint app.py
```

Envoyez votre code en revue en poussant votre code sur une branche :

```
$ git add .gitlab-ci.yml
$ git commit -m "Ajout d'un job testant la conformité pylint"
$ git push origin add_pipeline_conformity
```

> Question 6.2
>
> - Pourquoi le pipeline a t-il échoué ?    # les deps ne sont pas installées dans l'env et pylint râle

Le dépot que nous avons cloné contient un fichier `requirements.txt` qui contient la liste des dépendances
requises par notre application. Les erreurs renvoyées par Pylint nous informent que ces dépendances ne sont pas présentent
lors de l'execution de notre test, il nous faut donc installer ces dépendances.

En Python l'installation de librairies est possible avec la commande `pip` qui nous avons déjà utilisé dans notre pipeline.
Il est également possible d'utiliser `pip` en lui donnant un fichier contenant la liste des dépendances :

```
# pip install -r requirements.txt
```

Modifiez le pipeline pour utiliser cette commande à la place de nos appels pip précétents :

```yaml
variables:
  PYTHON3_IMAGE: python:alpine

stages:
  - syntax

PyCodeStyle:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
  - pip install -r requirements.txt     # < ici
  - pycodestyle app.py

Pylint:
  image: $PYTHON3_IMAGE
  stage: syntax
  script:
  - pip install -r requirements.txt     # < ici
  - pylint app.py
```

et effectuez un nouveau changement :

```
$ git add <lefichier>
$ git commit -m "Correction pip"
$ git push origin add_pipeline_conformity
```

Le pipeline ne doit maintenant plus renvoyer d'erreurs et en regardant le détail du job Pylint vous devriez avoir
un score de 10/10 !

## Un test orienté sécurité ?

Un test en lien avec la sécurité est possible a cette étape du developpement (et oui !). Ajoutons un test qui fera plaisir
à votre équipe de sécurité, en ajoutant l'execution de `bandit` (un outil d'analyse statique du code Python) dans un autre job.
Ce job ne sera pas executé en parallele des autres tests et nous allons ajouter une étape dédiée au pipeline appelée `sast` :

```yaml
stages:
- syntax
- sast

[...]

Static Code Analysis Tests:
  stage: sast
  image: $PYTHON3_IMAGE
  script:
  - pip install -r requirements.txt
  - bandit app.py
```

**Note :** SAST est l'accronyme signifiant Static Application Security Testing (SAST).

Ajoutez vos modifications dans un commit et poussez les sur votre branche :

```
$ git add .
$ git commit -m "Ajout d'un test SAST"
$ git push origin add_pipeline_conformity
```

> Question 6.3
>
> - Pourquoi le pipeline a t-il échoué ?    # bandit n'est pas dans le requirements.txt

Vous devriez être capable de corriger seuls le problème. Effectuez le correctif et soumettez le changement
à Gitlab.

## Création de tests unitaires pour notre application

Créez le fichier de tests suivant:

```python
# tests/test_sample1.py
from flask import request
from app import APP

def test_version():
    with APP.test_request_context('/version', method='GET'):
        assert request.path == '/version'
        assert request.method == 'GET'
```

Pour executer les tests, vous aurez besoin de la commande `pytest` que vous pouvez installer avec la commande suivante :

```
$ pip install pytest
```

Testez maintenant l'execution de vos tests :

```
$ python -m "pytest"
```

Vous devriez obtenir l'output suivante :

```
================================================================================= test session starts =================================================================================
platform darwin -- Python 3.7.4, pytest-5.2.4, py-1.8.0, pluggy-0.13.0
rootdir: /Users/remi.rey/dev/octo/gitlab-formation/ci-app
collected 1 item

tests/test_sample1.py .                                                                                                                                                         [100%]

================================================================================== 1 passed in 0.18s ==================================================================================
```

### Ajout du job executant les tests unitaires

Dans notre `.gitlab-ci.yml`, ajoutons un job executant la commande `pytest` que nous venons d'utiliser :

```yaml
stages:
  - syntax
  - sast
  - unittests

[...]
Launch Unit Tests:
  stage: unittests
  image: $PYTHON3_IMAGE
  script:
  - pip install -r requirements.txt
  - python -m pytest --junit-xml=pytest-report.xml
```

Nous ajoutons un `stage` qui viendra s'executer après les tests du stage `sast` dans lequel nous n'avons qu'un seul job executant `pytest`.
Ajoutez ce fichier a vos sources en effectuant un commit :

```
$ git add .gitlab-ci.yml
$ git commit -m "Ajout d'un stage pour les tests unitaires"
```

Envoyez votre code en revue en poussant votre code sur une branche :

```
$ git push origin add_pipeline_conformity
```

La commande `push` vous renvoie une URL permettant la création automatique d'une Merge Request. Cliquez sur le lien
et validez la création de la Merge Request. Surveillez l'évolution du pipeline.

> Question 6.4
>
> - Pourquoi le pipeline a t-il échoué ? Qu'avons-nous oublié de faire ?  # Ici ça échoue parcequ'on a pas pytest dans le requirements.txt

Corrigez le problème et soumettez votre changement. Le pipeline ne doit plus échouer.

**Tips : ** La version de `pytest` est récupérable dans votre environnement en executant la commande `pip show pytest`.

## Merge de votre changement

Ce TP est maintenant terminé. Dans un contexte réel il serait temps de demander une revue de votre code à l'un de vos pair.
Dans le cadre de cette formation, vous pouvez "merger" vous même vos changements.

Retournez dans l'interface Gitlab sur votre Merge Request et cliquez sur le bouton "Merge".
